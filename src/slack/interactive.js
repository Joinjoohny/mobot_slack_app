'use strict';

// Lib
const inform = require('./commands/inform/interactive');
const reminder = require('./commands/movies/interactive');
const preference = require('./commands/set/interactive');

// Main interceptor interactive messages
const interactiveMessages = (req, res) => {
  const q = JSON.parse(req.body.payload);

  if (q.token !== process.env.SLACK_VERIFICATION_TOKEN) {
    // the request is NOT coming from Slack!
    return;
  }
  const { callback_id } = q;

  switch (callback_id) {
    case 'inform_movie':
      inform(req, res);
      break;
    case 'reminder_movie':
      reminder(req, res);
      break;
    case 'set_preferences':
      preference(req, res);
      break;
    default:
      return res.json('Something went wrong :confused:');
  }
};

module.exports = interactiveMessages;
