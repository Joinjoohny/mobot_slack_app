'use strict';

// Lib
const async = require('async');
const moment = require('moment');
const _find = require('lodash').find;
// Modules
const reminder = require('../../controllers/reminder');
const moviesUtils = require('../utils/movies');
const { remind_worker } = require('../utils/messages');
const { typeOfDay } = require('../utils/helpers');

const reminderWorker = () => {
  // Get today informs
  async.parallel([
    cb => {
      reminder.getWorkerReminders((err, reminders) => {
        if (err) console.log(err);
        cb(null, reminders);
      });
    },
  ], (err, result) => {
    if (err) console.log(err);
    const [ reminders ] = result;
    // Proccess each reminder
    async.each(reminders, (reminder, done) => {
      const { movieId, teamId, userId, timeZone } = reminder;
      moviesUtils.getOneMovie(movieId, userId, (err, movie) => {
        const rtm = _find(global.rtm, { team: { id: teamId } });
        // Process timezone slack user
        const tz = timeZone / 3600;
        const time = moment().utcOffset(tz);
        const day = time.day();
        const hour = time.hour();
        // If rtm exist && day && hour
        if (rtm && day === reminder.day && typeOfDay(reminder.time, hour)) {
          // Send inform messages
          rtm.postMessage(userId, remind_worker, { attachments: [movie], as_user: true }).then(value => {
            // Remove reminder
            reminder.removeReminder({ userId, movieId });
            done();
          }, err => console.log(err));
        } else {
          done();
        }
      });
    });
  });
};

module.exports.reminderWorker = reminderWorker;
