'use strict';

//Lib
const moment = require('moment');
const async = require('async');
const _find = require('lodash').find;
// Modules
const inform = require('../../controllers/inform');
const moviesUtils = require('../utils/movies');
const { inform_worker } = require('../utils/messages');
const { typeOfDay } = require('../utils/helpers');

const informWorker = () => {
  // Get today informs
  async.waterfall([
    cb => { // Get genre list
      moviesUtils.getGenre(cb);
    },
    (genres, cb) => { // Get movie list and process each movie
      moviesUtils.getAttachments(genres, cb);
    },
  ], (err, result) => {
    if (err) console.log(err);
    // Get all informs which are scheduled for today
    inform.getWorkerInform((err, informs) => {
      if (err) console.log(err);
      // Prepare each inform
      async.each(informs, (inform, done) => {
        const { teamId, userId } = inform;
        const rtm = _find(global.rtm, { team: { id: teamId } });
        // Process timezone slack user
        const tz = inform.timeZone / 3600;
        const time = moment().utcOffset(tz);
        const day = time.day();
        const hour = time.hour();
        // If rtm exist && day && hour
        if (rtm && day === inform.day && typeOfDay(inform.time, hour)) {
          // Send inform messages
          rtm.postMessage(userId, inform_worker, { attachments: result, as_user: true }).then(value => {
            done();
          }, err => console.log(err));
        } else {
          done();
        }
      }, err => {
        if (err) console.log(err);
      });
    });
  });
};

module.exports.informWorker = informWorker;
