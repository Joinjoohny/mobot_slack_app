'use strict';

// Lib
const chalk = require('chalk');
const CronJob = require('cron').CronJob;
// Modules
const inform = require('./inform');
const reminder = require('./reminder');

const initWorker = () => {
  console.log(chalk.green('Worker started NOW.'));
  // Run jobs
  // reminder.reminderWorker();
  // inform.informWorker();
  // Run cron job every hour 1-5 day
  new CronJob('* * 00 * * 1-5', () => {
    console.log(chalk.green('Cron Job started NOW.'));
    inform.informWorker();
    reminder.reminderWorker();
  }, null, true, 'Europe/Kiev');
};

module.exports.initWorker = initWorker;
