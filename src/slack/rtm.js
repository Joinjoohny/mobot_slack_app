'use strict';
//Lib
const chalk = require('chalk');
const async = require('async');
const success = chalk.bold.green;
// Modules
const team = require('../controllers/team');
const slackBot = require('./bot');
const conversation = require('./conversation');
const worker = require('./worker');

team.getAllTeam((err, teams) => {
  if (err) console.log(err);
  // Declare global rtm
  global.rtm = [];

  async.each(teams, (item, cb) => {
    // Create connect RMT for each instances
    const bot = slackBot(item.accessTokenBot);
    // RTM is started
    bot.on('start', () => {
      global.rtm.push(bot);
      cb();
    });
    // RTM Event message
    bot.on('message', data => {
      if (data.type === 'message') {
        conversation(data);
      }
    });
    // RTM Error message
    bot.on('error', err => {
      if (err) console.error(err);

      setTimeout(() => {
        bot.login();
      }, 1000);
    });

  }, err => {
    if (err) console.error(err);
    // All instaces are stable
    console.log(success('RTM connected.'));
    worker.initWorker();
  });
});
