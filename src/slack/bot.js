'use strict';

const SlackBot = require('slackbots');

// create a bot
const createBot = token => {
  const bot = new SlackBot({
    token,
    name: 'MoBot'
  });

  return bot;
};

module.exports = createBot;
