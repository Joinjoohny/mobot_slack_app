'use strict';

// Lib
const _isString = require('lodash').isString;
const _find = require('lodash').find;
// Lib
const { hello_message, hello_attachments } = require('./utils/messages');

// Listener user messages
const conversation = data => {
  // console.log(data);
  const { team, user } = data;
  const hello = ['hi', 'hello', 'hey', 'bonjour', 'aloha', 'help'];
  const rtm = _find(global.rtm, { team: { id: team } });
  if (_isString(data.text) && rtm) {
    const text = data.text.toLowerCase();
    // Hello messages
    if (hello.includes(text)) {
      rtm.postMessage(user, hello_message, { attachments: hello_attachments, as_user: true }).then(value => {

      });
    }
  }
};

module.exports = conversation;
