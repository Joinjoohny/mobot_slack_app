'use strict';

// Lib
const async = require('async');
const _find = require('lodash').find;
// Modules
const moviesUtils = require('../../utils/movies');

// Get actual movie list
const getMovies = (req, res) => {
  const q = req.body;
  const { user_name, user_id, team_id, channel_id } = q;
  //console.log(q);

  if (q.token !== process.env.SLACK_VERIFICATION_TOKEN) {
  // the request is NOT coming from Slack!
    return;
  }
  async.waterfall([
    cb => { // Get genre list
      moviesUtils.getGenre(cb);
    },
    (genres, cb) => { // Get movie list and process each movie
      moviesUtils.getAttachments(user_id, genres, cb);
    },
  ], (err, result) => {
    if (err) return res.send('Ohhh sorry something went wrong :confused:');
    //res.status(200);
    const dataText = {
      response_type: 'in_channel', // public to the channle
      /* eslint-disable max-len */
      text: `Good Day ${user_name}! :slightly_smiling_face::v:\n *I want to _help_ you to have a _good time_!!!:tada: I choose for you fresh:green_apple:_movies_*:popcorn:\n *Enjoy!*`,
    };
    res.json(dataText);

    const rtm = _find(global.rtm, { team: { id: team_id } });
    if (rtm) {
      rtm.ws.send(JSON.stringify({ type: 'typing', channel: channel_id }));
      rtm.postMessage(user_id, '', { attachments: result, as_user: true }).then(value => {
      }, err => res.send('Error'));
    } else {
      res.send('Error');
    }
  });

};

module.exports = getMovies;
