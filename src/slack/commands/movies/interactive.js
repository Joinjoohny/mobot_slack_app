'use strict';

// Lib
const _head = require('lodash').head;
const _find = require('lodash').find;
const _map = require('lodash').map;
const async = require('async');
// Modules
const reminder = require('../../../controllers/reminder');
const { weekDays, informTime } = require('../inform/messages');
const moment = require('moment');

const interactiveMovies = (req, res) => {
  let data = {};
  let query = {};
  let attachments = [];
  const q = JSON.parse(req.body.payload);
  const { team, channel, message_ts, attachment_id, original_message, user } = q;
  const action = _head(q.actions);
  /* eslint-disable max-len */
  const mainMessage = `Good Day ${user.name}! :slightly_smiling_face::v:\n *I want to _help_ you to have a _good time_!!!:tada: I choose for you fresh:green_apple:_movies_*:popcorn:\n *Enjoy!*`;

  // RTM
  const rtm = _find(global.rtm, { team: { id: team.id } });
  if (!rtm) return res.json('Something went wrong :confused:');

  // What action ?
  switch (action.name) {
    // Yes, I will go button
    case 'reminder':
      // Update attachments
      const movieId = action.value;
      attachments = _map(original_message.attachments, item => {
        if (item.id == attachment_id) {
          const newActions = [];
          query = { movieName: item.title, teamId: team.id };
          // weekDays
          weekDays.forEach(item => {
            const { id, name } = item;
            const dayAction = {
              name: 'inform_week',
              text: name,
              type: 'button',
              value: `${id}_${movieId}`,
            };
            newActions.push(dayAction);
          });
          item.actions = newActions;
        }
        return item;
      });

      async.waterfall([
        cb => {
          // Update message
          rtm.updateMessage(channel.id, message_ts, mainMessage, { attachments }).then(value => {
            cb();
          }, err => cb('Error update message.'));
        },
        cb => { // Get all slack team user and find tz for current user
          rtm.getUsers().then(value => {
            const slackUser = _find(value.members, { id: user.id });
            cb(null, slackUser);
          }, err => {
            if (err) cb(err);
          });
        },
        (slackUser, cb) => {
          // Set timezone
          query.timeZone = slackUser.tz_offset;
          // Save reminder
          reminder.saveReminder({ userId: user.id, movieId, query }, (err, remiderData) => {
            if (err) cb('Error save reminder');
            cb(null, remiderData);
          });
        }
      ], (err, result) => {
        if (err) return res.json('Something went wrong :confused:');
        data = {
          response_type: 'in_channel',
          text: `Perfect, *${result.movieName}* it\'s a good choice! :wave: :popcorn:`,
          replace_original: false,
          delete_original: false,
        };
        res.json(data);
      });
      break;
    case 'inform_week':
      // Update attachments
      const [ day, weekMovie ] = action.value.split('_');
      attachments = _map(original_message.attachments, item => {
        if (item.id == attachment_id) {
          // Actions time
          const { actions } = _head(informTime.attachments);
          const inforTimeActions = [];
          actions.forEach(item => {
            const { name, text, type, value } = item;
            const timeAction = {
              name,
              text,
              type,
              value: `${value}_${weekMovie}`
            };
            inforTimeActions.push(timeAction);
          });
          item.actions = inforTimeActions;
        }
        return item;
      });

      async.waterfall([
        cb => {
          // Update message
          rtm.updateMessage(channel.id, message_ts, mainMessage, { attachments }).then(value => {
            cb();
          }, err => cb('Error update message.'));
        },
        cb => {
          // Save reminder
          query = { day: parseInt(day) };
          reminder.saveReminder({ userId: user.id, movieId: weekMovie, query }, (err, reminderData) => {
            if (err) cb('Error save reminder');
            cb(null, reminderData);
          });
        },
      ], (err, result) => {
        if (err) return res.json('Something went wrong :confused:');

        const day = moment().isoWeekday(result.day).format('dddd');
        data = {
          response_type: 'in_channel',
          text: `Great, I will remind you in *${day}* :spiral_calendar_pad:`,
          replace_original: false,
          delete_original: false,
        };
        res.json(data);
      });
      break;
    case 'inform_time':
      const [ time, timeMovie ] = action.value.split('_');
      query = { time };
    // Update attachments
      attachments = _map(original_message.attachments, item => {
        if (item.id == attachment_id) {
          item.actions = [];
        }
        return item;
      });

      async.waterfall([
        cb => {
          // Update message
          rtm.updateMessage(channel.id, message_ts, mainMessage, { attachments }).then(value => {
            cb();
          }, err => cb('Error update message.'));
        },
        cb => {
          reminder.saveReminder({ userId: user.id, movieId: timeMovie, query }, (err, reminderData) => {
            if (err) cb('Error save reminder');
            cb(null, reminderData);
          });
        },
      ], (err, result) => {
        if (err) return res.json('Something went wrong :confused:');
        // Message movie
        const day = moment().isoWeekday(result.day).format('dddd');
        const rememberMovie = `${result.movieName} - ${day}, ${result.time}`;
        data = {
          response_type: 'in_channel',
          text: `OOOKK, I remember :yum::ok_hand: *${rememberMovie}* :movie_camera:`,
          replace_original: false,
          delete_original: false,
        };
        res.json(data);
      });
      break;
    default:
      return res.json('Something went wrong :confused:');
  }
};

module.exports = interactiveMovies;
