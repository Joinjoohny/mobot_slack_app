'use strict';

// Lib
const _head = require('lodash').head;
const _find = require('lodash').find;
const moment = require('moment');
const async = require('async');
// Modules
const { informTime } = require('./messages');
const inform = require('../../../controllers/inform');

const interactiveInform = (req, res) => {
  let data = {};
  let query = {};
  const q = JSON.parse(req.body.payload);
  const action = _head(q.actions);
  const { user, team } = q;
  // What action ?
  switch (action.name) {
    case 'inform_week':
      data = informTime;
      async.waterfall([
        cb => { // Get all slack team user and find tz for current user
          const rtm = _find(global.rtm, { team: { id: team.id } });
          if (rtm) {
            rtm.getUsers().then(value => {
              const slackUser = _find(value.members, { id: user.id });
              cb(null, slackUser);
            }, err => {
              if (err) cb(err);
            });
          } else {
            cb('RTM not found.');
          }
        },
        (slackUser, cb) => {
          // Save inform
          query = { day: parseInt(action.value), teamId: team.id, timeZone: slackUser.tz_offset };
          inform.saveInform({ userId: user.id, query }, (err, inform) => {
            if (err) return cb(err);
            cb();
          });
        },
      ], (err, result) => {
        if (err) return res.send('Error');
        res.json(data);
      });
      break;
    case 'inform_time':
      query = { time: action.value };
      inform.saveInform({ userId: user.id, query }, (err, inform) => {
        if (err) return res.send('Error');
        const day = moment().isoWeekday(inform.day).format('dddd');
        data = {
          response_type: 'in_channel', // public to the channel
          /* eslint-disable max-len */
          text: `Perfect!!! :clap::skin-tone-3: I will inform you on :spiral_calendar_pad: *${day} ${inform.time}*!`,
          attachments: [
            {
              text: ':bulb: Hint, if you want to forget about our agreement use \`\/inform cancel\`',
              mrkdwn_in: ['text']
            }
          ]
        };
        res.json(data);
      });
      break;
    default:
      return res.json('Something went wrong :confused:');
  }
};

module.exports = interactiveInform;
