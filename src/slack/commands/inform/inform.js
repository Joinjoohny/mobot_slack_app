'use strict';

// Modules
const { weekDays } = require('./messages');
// Lib
const inform = require('../../../controllers/inform');
const { inform_cancel, inform_cancel_tips } = require('../../utils/messages');
// Inform user about the movie
const getInform = (req, res) => {
  const q = req.body;
  const { user_id } = q;
  let data = {};

  if (q.token !== process.env.SLACK_VERIFICATION_TOKEN) {
  // the request is NOT coming from Slack!
    return;
  }

  const [ first ] = q.text.split(/[\s,]+/); // By space

  if (first && first.includes('cancel')) {
    inform.cancelInform({ userId: user_id }, err => {
      if (err) return res.send('Error');
      data = {
        response_type: 'in_channel', // public to the channel
        text: inform_cancel,
        attachments: [{ text: inform_cancel_tips, mrkdwn_in: ['text']}]
      };
      res.send(data);
    });
  } else {
    const actions = [];
    // Proccess actions
    weekDays.forEach(item => {
      const { id, name } = item;
      const dayAction = {
        name: 'inform_week',
        text: name,
        type: 'button',
        value: id,
      };
      actions.push(dayAction);
    });

    data = {
      response_type: 'in_channel', // public to the channle
      attachments: [
        {
          callback_id: 'inform_movie',
          text: 'What day of the week for inform about new movie? :popcorn: :date:',
          actions,
        }
      ],
    };
    res.send(data);
  }
};

module.exports = getInform;
