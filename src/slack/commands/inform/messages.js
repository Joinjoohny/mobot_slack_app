'use strict';

module.exports = {
  weekDays:  [
    { id: 1, name: 'Monday' },
    { id: 2, name: 'Tuesday' },
    { id: 3, name: 'Wednesday' },
    { id: 4, name: 'Thursday' },
    { id: 5, name: 'Friday' },
  ],
  informTime: {
    response_type: 'in_channel',
    attachments: [
      {
        text: 'What is the most appropriate time?',
        callback_id: 'inform_movie',
        fallback: 'Fallback',
        actions: [
          {
            name: 'inform_time',
            text: 'Morning :clock730:',
            type: 'button',
            value: 'morning',
          },
          {
            name: 'inform_time',
            text: 'Afternoon :clock2:',
            type: 'button',
            value: 'afternoon',
          },
          {
            name: 'inform_time',
            text: 'Evening :clock9:',
            type: 'button',
            value: 'evening',
          },
        ]
      }
    ]
  },
};
