'use strict';

// Modules
const _chunk = require('lodash').chunk;
// Lib
const { location } = require('../../utils/settings');
const { preferences_default } = require('../../utils/messages');

const setPreferences = (req, res) => {
  const q = req.body;
  //console.log(q);

  if (q.token !== process.env.SLACK_VERIFICATION_TOKEN) {
  // the request is NOT coming from Slack!
    return;
  }
  const [ first ] = q.text.split(/[\s,]+/); // By space

  if (first && first.includes('default')) {
    // TODO: Delete preferences
    const data = {
      response_type: 'in_channel', // public to the channel
      text: preferences_default,
    };
    return res.json(data);
  } else {
    const actions = location.map(item => {
      const action = {
        name: 'location',
        text: `${item.name} ${item.icon}`,
        type: 'button',
        value: item.id,
        style: 'primary'
      };
      return action;
    });
    // Chunk location for 5
    const chunkActions = _chunk(actions, 5);
    const attachments = [
      {
        callback_id: 'set_preferences',
        text: '',
        actions: chunkActions[0]
      }
    ];
    // Prepate location for new attachments
    const otherLocation = chunkActions.slice(1);
    otherLocation.forEach(item => {
      const otherAttachemnt = {
        callback_id: 'set_preferences',
        text: '',
        actions: item
      };
      attachments.push(otherAttachemnt);
    });
    // First data
    const data = {
      response_type: 'in_channel', // public to the channel
      text: 'Choose your location :earth_americas:',
      attachments
    };
    res.json(data);
  }
};

module.exports = setPreferences;
