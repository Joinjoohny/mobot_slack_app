'use strict';

// Modules
const _head = require('lodash').head;
// Lib
const { languages } = require('../../utils/settings');
const { preferences_finish } = require('../../utils/messages');
const settings = require('../../../controllers/settings');

const interactivePreference = (req, res) => {
  let data = {};
  let query = {};
  const q = JSON.parse(req.body.payload);
  const action = _head(q.actions);
  const { user } = q;
  // What action?
  switch (action.name) {
    case 'location':
      const actions = languages.map(item => {
        const action = {
          name: 'language',
          text: `${item.name} ${item.icon}`,
          type: 'button',
          value: item.id,
          style: 'primary'
        };
        return action;
      });
      data = {
        response_type: 'in_channel',
        text: 'Select comfortable languages :loudspeaker:',
        attachments: [{ text: '', callback_id: 'set_preferences', actions }]
      };
      query = { region: action.value };
      settings.saveSettings({ userId: user.id, query }, (err, preferences) => {
        if (err) return res.json('Something went wrong :confused:');
        res.json(data);
      });
      break;
    case 'language':
      query = { language: action.value };
      settings.saveSettings({ userId: user.id, query }, (err, preferences) => {
        if (err) return res.json('Something went wrong :confused:');
        const location = preferences.region.toLowerCase();
        const lang = (preferences.language === 'en') ? 'us' : preferences.language;
        data = {
          response_type: 'in_channel',
          text: `Perfect!!!:clap: \n *Location* - :flag-${location}: \n *Language* - :flag-${lang}:`,
          attachments: [
            {
              text: preferences_finish,
              mrkdwn_in: ['text']
            }
          ]
        };
        res.json(data);
      });
      break;
    default:
      return res.json('Something went wrong :confused:');
  }
};

module.exports = interactivePreference;
