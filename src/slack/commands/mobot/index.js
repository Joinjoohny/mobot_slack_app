'use strict';

// Modules
const feedback = require('./feedback');
const help = require('./help');
const { hello_message, hello_attachments } = require('../../utils/messages');
const { typingBot } = require('../../utils/helpers');

const mobot = (req, res) => {
  const q = req.body;
  const { team_id, channel_id } = q;

  if (q.token !== process.env.SLACK_VERIFICATION_TOKEN) {
  // the request is NOT coming from Slack!
    return;
  }
  // Send typing
  typingBot(team_id, channel_id);

  const [ first ] = q.text.split(/[\s,]+/); // By space
  if (first.includes('help')) {
    // Help
    help(req, res);
  } else if (first.includes('feedback')) {
    // Feedback
    feedback(req, res);
  } else {
    // Welcome
    const data = {
      response_type: 'in_channel', // public to the channel
      text: hello_message,
      attachments: hello_attachments
    };
    res.json(data);
  }
};

module.exports = mobot;
