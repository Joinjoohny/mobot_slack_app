'use strict';

// Modules
const nodemailer = require('nodemailer');

const feedback = (req, res) => {
  const q = req.body;
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'mobot.feedback@gmail.com',
      pass: '1118047mobot'
    }
  });
  const feedback = q.text.split(/[\s]+/).slice(1);
  // No messages
  if (!feedback.length) return res.send('Please, write a message for me, I read it with pleasure :blush:');
  // Setup email data with unicode symbols
  const mailOptions = {
    from: '"Mobot Feedback 👻" <mobot.feedback@gmail.com>', // sender address
    to: 'joinjoohny@gmail.com', // list of receivers
    subject: 'Mobot Feedback ✔', // Subject line
    text: feedback.join(' '), // plain text body
  };

  // send mail with defined transport object
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.log(error);
      return res.send('Sorry, something went wrong. Try again! :sweat:');
    }
    console.log('Message %s sent: %s', info.messageId, info.response);
    res.send('Thank you! I got your message. I\'ll do my best :muscle:');
  });
};

module.exports = feedback;
