'use strict';
// Lib
const request = require('request');
const chalk = require('chalk');
// Modules
const team = require('../controllers/team');
const { slackOauth, teamInfo } = require('./utils/settings');
const success = chalk.bold.green;
// Ouath callback
const oauth = (req, res) => {
  if (!req.query.code) { // access denied
    res.redirect('index');
    return;
  }
  // Access data
  const data = {form: {
    client_id: process.env.SLACK_CLIENT_ID,
    client_secret: process.env.SLACK_CLIENT_SECRET,
    code: req.query.code
  }};

  request.post(slackOauth, data, (error, response, body) => {
    if (!error && response.statusCode == 200) {
      // Get an auth token
      const accessToken = JSON.parse(body).access_token;
      const accessTokenBot = JSON.parse(body).bot.bot_access_token;

      // Get the team domain name to redirect to the team URL after auth
      request.post(teamInfo, {form: { token: accessToken }}, (error, response, body) => {
        if (!error && response.statusCode == 200) {
          if (JSON.parse(body).error == 'missing_scope') {
            res.send('Missing scope!');
          } else {
            const { domain, id } = JSON.parse(body).team;
            team.saveTeam({ teamId: id, domain, accessToken, accessTokenBot }, (err, team) => {
              if (err) console.log(err);
              console.log(success(`New team implemented - ${domain}`));
            });
            res.redirect('http://' + domain + '.slack.com');
          }
        }
      });
    }
  });
};

module.exports = oauth;
