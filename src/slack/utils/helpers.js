'use strict';

// Modules
const _find = require('lodash').find;
// Helpers function
module.exports = {
  typeOfDay: (type, hour) => {
    if (type === 'morning') {
      return (hour >= 9 && hour < 10);
    }
    if (type === 'afternoon') {
      return (hour >= 13 && hour < 14);
    }
    if (type === 'evening') {
      return (hour >= 17 && hour < 18);
    }
  },
  typingBot: (id, channel) => {
    const rtm = _find(global.rtm, { team: { id } });
    if (rtm) {
      rtm.ws.send(JSON.stringify({ type: 'typing', channel }));
    }
  },
};
