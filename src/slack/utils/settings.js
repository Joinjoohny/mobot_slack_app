'use strict';

module.exports = {
  slackOauth: 'https://slack.com/api/oauth.access',
  teamInfo: 'https://slack.com/api/team.info',
  moviedbToken: 'c52beae40da1ac325609a8c2a395e8af',
  api: {
    moviesList: 'https://api.themoviedb.org/3/discover/movie',
    genreList: 'https://api.themoviedb.org/3/genre/movie/list',
    getMovie: 'https://api.themoviedb.org/3/movie',
  },
  location: [
    { id: 'US', name: 'USA', icon: ':flag-us:' },
    { id: 'CA', name: 'Canada', icon: ':flag-ca:' },
    { id: 'GB', name: 'Great Britain', icon: ':flag-gb:' },
    { id: 'FR', name: 'France', icon: ':flag-fr:' },
    { id: 'DE', name: 'Germany', icon: ':flag-de:' },
    //
    { id: 'ES', name: 'Spain', icon: ':flag-es:' },
    { id: 'RU', name: 'Russia', icon: ':flag-ru:' },
    { id: 'UA', name: 'Ukraine', icon: ':flag-ua:' },
    { id: 'IT', name: 'Italy', icon: ':flag-it:' },
    { id: 'CN', name: 'China', icon: ':flag-cn:' },
    //
    { id: 'NL', name: 'Netherlands', icon: ':flag-nl:' },
    { id: 'AU', name: 'Australia', icon: ':flag-au:' },
    { id: 'BR', name: 'Brazil', icon: ':flag-br:' },
    { id: 'JP', name: 'Japan', icon: ':flag-jp:' },
    { id: 'IN', name: 'India', icon: ':flag-in:' },
  ],
  languages: [
    { id: 'en', name: 'English', icon: ':flag-us:' },
    { id: 'ru', name: 'Russian', icon: ':flag-ru:' },
    { id: 'fr', name: 'French', icon: ':flag-fr:' },
    { id: 'es', name: 'Spanish', icon: ':flag-es:' },
    { id: 'cn', name: 'Chinese', icon: ':flag-cn:' },
  ],
};
