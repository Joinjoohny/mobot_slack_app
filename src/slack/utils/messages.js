'use strict';

module.exports = {
  // Inform
  /* eslint-disable max-len */
  inform_cancel: 'All right, I will not inform about new movies. :mask:',
  // Inform cancel
  inform_cancel_tips: ':bulb: Hint, at any time, you can ask me again to `/inform`. Good luck! :ok_hand::skin-tone-3: :popcorn:',
  // Inform worker
  inform_worker: 'Hey :wink: I want to remind you about movie :clapper: Have a nice day and see you soon :v::skin-tone-3:',
  // Remind worker
  remind_worker: 'Hey :wink: You wanted to go a cinema :clapper: *Enjoy watching* :popcorn: :pizza: :beer:',
  // Hello
  hello_message: 'Welcome my friend :relaxed: I can inform you about fresh films from the cinema world :movie_camera: notify and remind :watch: by language you speak and depending on location you live :earth_americas:',
  hello_attachments: [
    { // Movies
      text: ' :bulb: Use `/movies` :point_right::skin-tone-3: Find out fresh films with a detailed description, rating and more… \n If you want, I can remind what picture you would like to see :sunglasses:',
      mrkdwn_in: ['text']
    },
    { // Inform
      text: ' :bulb: Use `/inform` :point_right::skin-tone-3: All you need todo - is choose comfortable time for delivery notification :date: \n At any time you can disable notification, just use `/inform cancel` :zipper_mouth_face:',
      mrkdwn_in: ['text']
    },
    { // Set
      text: ' :bulb: Use `/set` :point_right::skin-tone-3: To get a more accurate list of movies, select a specific country :world_map: and language :loudspeaker: \n Also, you can always set up the default preferences `/set default` :gear:',
      mrkdwn_in: ['text']
    },
    { // Mobot
      text: ' :bulb: Use `/mobot help` :point_right::skin-tone-3: Detailed information, how to communicate with me :joystick: \n Any request or suggestion, your welcome `/mobot feedback` :postbox:',
      mrkdwn_in: ['text']
    }
  ],
  // Preferences
  preferences_default: 'Nice :clap: You preferences set as default!',
  preferences_finish: ':bulb: Hint, if you want to set up the default preferences use `/set default`',
  // Other
  error: 'Something went wrong :confused:',
};
