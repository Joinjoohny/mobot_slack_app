'use strict';

// Modules
const moment = require('moment');
const request = require('request');
const _filter = require('lodash').filter;
const _map = require('lodash').map;
const async = require('async');
// Lib
const { moviedbToken, api } = require('./settings');
const settings = require('../../controllers/settings');

module.exports = {
  getGenre: cb => { // Get genre list
    request.get(api.genreList, {
      qs: { api_key: moviedbToken } }, (err, response, body) => {
        if (err) return cb(err);
        const { genres } = JSON.parse(body);
        cb(null, genres);
      });
  },
  getAttachments: (userId, genres, done) => { // Get movies and prepare as attachment
    async.waterfall([
      cb => { // Get user preferences
        settings.getSettings(userId, (err, preferences) => {
          if (err) cb('Error get settings');
          const userPreferences = (preferences) ? preferences : { language: 'en', region: 'US' };
          cb(null, userPreferences);
        });
      },
      ({ region, language }, cb) => { // Get fresh movies list
        // Build query
        const start = moment().subtract(1, 'month').format('YYYY-MM-DD');
        const end = moment().format('YYYY-MM-DD');
        const qs = {
          api_key: moviedbToken,
          region,
          language,
          'primary_release_date.gte': start,
          'primary_release_date.lte': end,
          'vote_average.gte': 5,
          page: 1,
        };
        // Get actual movies
        request.get(api.moviesList, { qs }, (err, response, body) => {
          if (err) return cb(err);
          // Attach
          const attachments = [];
          const movies = JSON.parse(body);
          const dataMovies = movies.results.slice(0, 5);
          dataMovies.forEach(movie => {
            // Genres
            const filterGenre = _filter(genres, item => (movie.genre_ids.includes(item.id)));
            const fieldGenre = _map(filterGenre, 'name');
            // Adult
            const adult = (movie.adult) ? 'Yes' : 'No';
            // Attach each movie
            const attachMovie = {
              // color: '#F1B23E',
              title: movie.title.toUpperCase(),
              text: `${movie.overview}`,
              // TODO Image url proxy
              image_url: `https://image.tmdb.org/t/p/w500/${movie.poster_path}`,
              fields: [
                {
                  title: 'Release Date :date:',
                  value: movie.release_date,
                  short: true,
                },
                {
                  title: 'Adult :couple:',
                  value: adult,
                  short: true,
                },
                {
                  title: 'Vote :ballot_box_with_ballot:',
                  value: movie.vote_average,
                  short: true,
                },
                {
                  title: 'Genre :movie_camera:',
                  value: fieldGenre.join(', '),
                  short: true,
                }
              ],
              actions: [
                {
                  name: 'reminder',
                  text: 'Yes, I will go! :+1:',
                  type: 'button',
                  value: movie.id,
                  style: 'primary'
                }
              ],
              callback_id: 'reminder_movie',
            };
            attachments.push(attachMovie);
          });
          cb(null, attachments);
        });
      },
    ], (err, result) => {
      if (err) done(err);
      done(null, result);
    });
  },
  getOneMovie: (id, userId, done) => {
    async.waterfall([
      cb => { // Get user preferences
        settings.getSettings(userId, (err, preferences) => {
          if (err) cb('Error get settings');
          const userPreferences = (preferences) ? preferences : { language: 'en', region: 'US' };
          cb(null, userPreferences);
        });
      },
      ({ region, language }, cb) => { // Get one movie
        request.get(`${api.getMovie}/${id}`, {
          qs: { api_key: moviedbToken, language, region } }, (err, response, body) => {
            if (err) return cb(err);
            const movie = JSON.parse(body);
            const fieldGenre = _map(movie.genres, 'name');
            // Adult
            const adult = (movie.adult) ? 'Yes' : 'No';
            // Buidl attachment movie
            const attachMovie = {
              title: movie.title.toUpperCase(),
              text: `${movie.overview}`,
              // TODO Image url proxy
              image_url: `https://image.tmdb.org/t/p/w500/${movie.poster_path}`,
              fields: [
                {
                  title: 'Release Date :date:',
                  value: movie.release_date,
                  short: true,
                },
                {
                  title: 'Adult :couple:',
                  value: adult,
                  short: true,
                },
                {
                  title: 'Vote :ballot_box_with_ballot:',
                  value: movie.vote_average,
                  short: true,
                },
                {
                  title: 'Genre :movie_camera:',
                  value: fieldGenre.join(', '),
                  short: true,
                }
              ],
            };
            cb(null, attachMovie);
          });
      },
    ], (err, result) => {
      if (err) done(err);
      // Done attachMovie
      done(null, result);
    });
  }
};
