'use strict';
/**
 * Schema Definitions
 *
 */
const mongoose = require('mongoose');

const SettingSchema = new mongoose.Schema({
  userId: String,
  region: String,
  language: String,
}, { timestamps: true });

module.exports = mongoose.model('setting', SettingSchema);
