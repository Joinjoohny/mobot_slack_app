'use strict';
/**
 * Schema Definitions
 *
 */
const mongoose = require('mongoose');

const ReminderSchema = new mongoose.Schema({
  userId: String,
  teamId: String,
  day: Number,
  time: String,
  timeZone: Number,
  movieId: Number,
  movieName: String,
  date: { type: Date, default: Date.now }
});

module.exports = mongoose.model('reminder', ReminderSchema);
