'use strict';
/**
 * Schema Definitions
 *
 */
const mongoose = require('mongoose');

const InformSchema = new mongoose.Schema({
  userId: String,
  teamId: String,
  day: Number,
  time: String,
  timeZone: Number,
  date: { type: Date, default: Date.now }
}, { timestamps: true });

module.exports = mongoose.model('inform', InformSchema);
