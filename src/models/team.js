'use strict';
/**
 * Schema Definitions
 *
 */
const mongoose = require('mongoose');

const TeamSchema = new mongoose.Schema({
  teamId: String,
  domain: String,
  accessToken: String,
  accessTokenBot: String,
  date: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Team', TeamSchema);
