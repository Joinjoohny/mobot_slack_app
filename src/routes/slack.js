'use strict';
// Lib
const express = require('express');
const router = express.Router();
// Modules
const slackOauth = require('../slack/oauth');
// Commands
const movies = require('../slack/commands/movies/movies');
const inform = require('../slack/commands/inform/inform');
const set = require('../slack/commands/set');
const mobot = require('../slack/commands/mobot');
// Others
const interactive = require('../slack/interactive');

// Oauth
router.get('/oauth', slackOauth);
// Slash commands
router.post('/movies', movies);
router.post('/inform', inform);
router.post('/set', set);
router.post('/mobot', mobot);
// Ohters
router.post('/interactive', interactive);

module.exports = router;
