'use strict';

const Settings = require('../models/settings');

module.exports = {
  saveSettings: ({ userId, query }, cb) => {
    Settings.findOneAndUpdate({ userId }, query,
      { upsert: true, new: true }, (err, preferences) => {
        if (err) cb(err);
        cb(null, preferences);
      });
  },
  getSettings: (userId, cb) => {
    Settings.findOne({ userId }, (err, preferences) => {
      if (err) cb(err);
      cb(null, preferences);
    });
  },
  removeSettings: (userId, cb) => {
    Settings.remove({ userId }, err => {
      if (err) cb(err);
      cb(null);
    });
  },
};
