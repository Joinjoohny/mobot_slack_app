'use strict';

const Inform = require('../models/inform');

module.exports = {
  saveInform: ({ userId, query }, cb) => {
    Inform.findOneAndUpdate({ userId }, query,
      { upsert: true, new: true }, (err, inform) => {
        if (err) cb(err);
        cb(null, inform);
      });
  },
  cancelInform: ({ userId }, cb) => {
    Inform.remove({ userId }, err => {
      if (err) cb(err);
      cb(null);
    });
  },
  getLaunchInform: cb => {
    Inform.find((err, teams) => {
      if (err) cb(err);
      cb(null, teams);
    });
  },
  getWorkerInform: cb => {
    Inform.find({}, (err, teams) => {
      if (err) cb(err);
      cb(null, teams);
    });
  },
};
