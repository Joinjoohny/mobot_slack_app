'use strict';

const Reminder = require('../models/reminder');

module.exports = {
  saveReminder: ({ userId, movieId, query }, cb) => {
    Reminder.findOneAndUpdate({ userId, movieId }, query,
      { upsert: true, new: true }, (err, reminder) => {
        if (err) cb(err);
        cb(null, reminder);
      });
  },
  getReminders: cb => {
    Reminder.find((err, reminders) => {
      if (err) cb(err);
      cb(null, reminders);
    });
  },
  getWorkerReminders: cb => {
    Reminder.find({}, (err, reminders) => {
      if (err) cb(err);
      cb(null, reminders);
    });
  },
  removeReminder: ({ userId, movieId }, cb) => {
    Reminder.remove({ userId, movieId }, err => {
      if (err) cb(err);
      cb(null);
    });
  },
};
