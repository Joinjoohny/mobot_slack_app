'use strict';

const Team = require('../models/team');

module.exports = {
  saveTeam: ({ teamId, domain, accessToken, accessTokenBot }, cb) => {
    Team.findOneAndUpdate({ teamId }, {
      teamId,
      domain,
      accessToken,
      accessTokenBot
    },
      { upsert: false, new: true }, (err, team) => {
        if (err) cb(err);
        cb(null, team);
      });
  },
  getAllTeam: cb => {
    Team.find((err, teams) => {
      if (err) cb(err);
      cb(null, teams);
    });
  },
};
